from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import home

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time

# Create your tests here.

class Story8UnitTest(TestCase):
	
	def test_story_8_url_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)	

	def test_story_7_using_home_func(self):
		found = resolve('/')
		self.assertEqual(found.func, home)

class Story8FunctionalTest(TestCase):

	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')        
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(Story8FunctionalTest, self).setUp()

	def tearDown(self):
		self.selenium.quit()
		super(Story8FunctionalTest, self).tearDown()

	def test_up_down(self):
		selenium = self.selenium

		selenium.get('http://127.0.0.1:8000/')
		
		time.sleep(5)

		# find the form element
		up = selenium.find_element_by_css_selector('a.up')
		down = selenium.find_element_by_css_selector('a.down')

		down.click()
		time.sleep(5)
		up.click()

		time.sleep(5)

	def test_accordion(self):
		selenium = self.selenium
		selenium.get('http://127.0.0.1:8000/')
		time.sleep(3)
		accordion = selenium.find_element_by_class_name('one')
		accordion.click()
		time.sleep(3)